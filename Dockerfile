#builder
FROM golang:alpine as builder
WORKDIR /home
COPY . .
RUN go build -o go-http-req main.go

#final image
FROM alpine
RUN apk update && \
    apk add --no-cache tzdata && \
    apk add --no-cache curl
ENV TZ=Asia/Jakarta
RUN rm -rf /var/cache/apk/* && date
COPY --from=builder /home/go-http-req .
EXPOSE 5050
CMD ./go-http-req