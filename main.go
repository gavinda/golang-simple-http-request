package main

import (
	"io/ioutil"
	"log"
	"net/http"
)

var httpClient http.Client

func main() {
	http.HandleFunc("/", GetUsers)
	log.Println("app running on localhost:8181")
	http.ListenAndServe(":8181", nil)
}

// type response struct {
// 	Status  int    `json:"status"`
// 	Message string `json:"message"`
// 	Data    struct {
// 		List []struct {
// 			ID    int    `json:"id"`
// 			Name  string `json:"name"`
// 			Grade int    `json:"grade"`
// 		} `json:"list"`
// 	} `json:"data"`
// }

func GetUsers(w http.ResponseWriter, r *http.Request) {
	// var responseStudent response
	req, err := http.NewRequest(http.MethodGet, "http://host.docker.internal:5050/v1/student", nil)
	if err != nil {
		log.Println(err)
		responseFailed(w)
	}
	resp, err := httpClient.Do(req)
	if err != nil {
		log.Println(err)
		responseFailed(w)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println(err)
		responseFailed(w)
	}
	log.Println("response : ", string(body))
	responseSuccess(w)
}

func responseFailed(w http.ResponseWriter) {
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte("failed"))
}

func responseSuccess(w http.ResponseWriter) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("success"))
}
